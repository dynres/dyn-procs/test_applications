#! /usr/bin/python3

import os
import sys

#
# set possible variables
#
vars = Variables()


# SWE specific variables
vars.AddVariables(
  PathVariable( 'buildDir', 'where to build the code', 'build', PathVariable.PathIsDirCreate ),

  EnumVariable( 'example', 'which application to build', 'mpidynres2d',
                allowed_values=(
                    'mpidynres2d',
                    'DynMPISessions_v2a', 'DynMPISessions_v2a_nb',
                    'DynMPISessions_v2b', 'DynMPISessions_v2b_nb',
                    'DynMPISessions_v2a_replace', 'DynMPISessions_v2a_sleep',
                    'DynMPISessions_minimal_add', 'DynMPISessions_v2a_fortran',
                    'DynMPISessions_minimal_split'
                    )
              ),

  EnumVariable( 'compileMode', 'whether to compile with debug options', 'release',
                allowed_values=('debug', 'release', 'sanitized')
              ),

  BoolVariable( 'netCDF', 'whether to use the netCDF library (required for reading scenario data from files)', 0),

  PathVariable( 'netCDFDir', 'location of netCDF', None)
)


# set environment
env = Environment(variables=vars, ENV = { 'PATH' : os.environ['PATH'],
                                          'HOME' : os.environ['HOME']
                                          })

# generate help text
Help("""Compile the example applications for dynamic resource management on p4est.
The following options are available:\n""" + vars.GenerateHelpText(env))

# handle unknown, maybe misspelled variables
unknownVariables = vars.UnknownVariables()

# exit in the case of unknown variables
if unknownVariables:
  print >> sys.stderr, "*** The following build variables are unknown:", unknownVariables.keys()
  Exit(1)

#
# precompiler, compiler and linker flags
#
env['CXX'] = env['LINKERFORPROGRAMS'] = env.Detect(['mpiCC', 'mpicxx'])
env['CXX'] = env['LINKERFORPROGRAMS'] = '/usr/bin/g++'
# set (pre-)compiler flags for the compile modes
if env['compileMode'] == 'debug':
  env.Append(CPPDEFINES=['DEBUG'])
  env.Append(CCFLAGS=['-O0','-g3','-Wall', '-fno-inline', '-std=gnu++11', '-fsanitize=address'])

elif env['compileMode'] == 'release':
  env.Append(CPPDEFINES=['NDEBUG'])
  env.Append(CCFLAGS=['-O3','-mtune=native', '-std=gnu++11'])
  #env.Append(CCFLAGS=['-O1','-mtune=native', '-std=gnu++11'])

#
# Same for Fortran
#
env['F90'] = env.Detect('mpifort')
env['FORTRAN'] = env['F90']
env['F90MODDIR'] = '/tmp'
env['FORTRANMODDIR'] = '/tmp'

# general flags
fortran_flags = ["-ffree-line-length-none", "-cpp"]
# warnings
fortran_flags += ["-Wall", "-extra", "-Wpedantic", "-Wconversion", "-fcheck=all"]

if env['compileMode'] == 'debug':
  fortran_flags += ['-O0','-ggdb','-Wall', '-fno-inline', "-ffpe-trap=zero,overflow,underflow"]

elif env['compileMode'] == 'release':
  fortran_flags += ['-O3','-mtune=native']

elif env['compileMode'] == 'sanitized':
  fortran_flags += ['-fsanitize=address', '-O0','-ggdb','-Wall', '-fno-inline', "-ffpe-trap=zero,overflow,underflow"]

env.Append(F90FLAGS=fortran_flags)

# path of project root
project_root = os.getcwd()

# Add source directory to include path (important for subdirectories)
env.Append(CPPPATH=['.'])
env.Append(CPPPATH=['include'])

try:
  CPATH = list(filter(None, os.environ['CPATH'].split(':')))
  for path in CPATH:
    env.Append(CPATH=[path])
except: 
	pass

try:
  CPPPATH = list(filter(None, os.environ.get('C_INCLUDE_PATH').split(':')))
  for path in CPPPATH:
    env.Append(CPPPATH=[path])
except:
  pass

env.Append(LIBPATH=['.'])
env.Append(LIBPATH=['include'])

try:
  LD_LIBRARY_PATH = list(filter(None, os.environ.get('LD_LIBRARY_PATH').split(':')))
  for path in LD_LIBRARY_PATH:
    env.Append(LIBPATH=[path])
except:
  pass

if env['compileMode'] == 'sanitized':
  env.Append(CXXFLAGS=['-fsanitize=address'])
  env.Append(CCFLAGS=['-fsanitize=address'])

#env.Append(CXXFLAGS=[os.environ.get('CXXFLAGS').split(' ') if ])
#env.Append(CCFLAGS=[os.environ.get('CCFLAGS').split(' ')])

# link with p4est, libsc, and libmpidynres
if env['compileMode'] == 'sanitized':
  env.Append(LINKFLAGS=['-fsanitize=address'])
  env.Append(LINKFLAGS=['-lmpi' , '-lasan'])
else:
  env.Append(LINKFLAGS=['-lmpi']) #,'-lmpidynres'
print(env.Dump())


#
# setup the program name and the build directory
#
#program_name = 'SWE_p4est'

program_name = env['example']

program_name += '_'+env['compileMode']

# build directory
build_dir = env['buildDir']+'/build_'+program_name

# get the src-code files
env.src_files = []

if env['example'] == 'DynMPISessions_v2a':
  sourceFiles = ['examples/dyn_mpi_sessions_v2a.cpp']
elif env['example'] == 'DynMPISessions_v2a_nb':
  sourceFiles = ['examples/dyn_mpi_sessions_v2a_nb.cpp']
elif env['example'] == 'DynMPISessions_v2b':
  sourceFiles = ['examples/dyn_mpi_sessions_v2b.cpp']
elif env['example'] == 'DynMPISessions_v2b_nb':
  sourceFiles = ['examples/dyn_mpi_sessions_v2b_nb.cpp']
elif env['example'] == 'DynMPISessions_v2a_replace':
  sourceFiles = ['examples/dyn_mpi_sessions_v2a_replace.cpp']
elif env['example'] == 'DynMPISessions_v2a_sleep':
  sourceFiles = ['examples/dyn_mpi_sessions_v2a_sleep.cpp']
elif env['example'] == 'DynMPISessions_minimal_add':
  sourceFiles = ['examples/dyn_mpi_session_minimal_add.cpp']
elif env['example'] == 'DynMPISessions_minimal_split':
  sourceFiles = ['examples/dyn_mpi_session_minimal_split.cpp']
elif env['example'] == 'DynMPISessions_v2a_fortran':
  sourceFiles = ['examples/util.f90', 'examples/dyn_mpi_sessions_v2a.f90']

if any(sourceFile.endswith('.f90') for sourceFile in sourceFiles):
  # for fortran programs, don't build object files, compile directly
  for sourceFile in sourceFiles:
    env.src_files.append(sourceFile)
else:
  for sourceFile in sourceFiles:
    env.src_files.append(env.Object(sourceFile))

# build the program
env.Program('build/'+program_name, env.src_files)

module util
    use iso_c_binding
    implicit none

    interface
        function atoi(str) bind(c, name="atoi")
            use, intrinsic :: iso_c_binding
            implicit none
            character(kind=c_char), dimension(*), intent(in) :: str
            integer(c_int) :: atoi
        end function atoi
    end interface

  contains

    subroutine stopp(file,Nline,msg, val, rank)
        character(len=*), intent(in) :: file
        integer, intent(in):: Nline
        character(len=*), intent(in) :: msg
        integer, intent(in), optional :: val
        integer, intent(in), optional :: rank

        print *,'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        if (present(rank))   print *,'Rank = ',rank
        print *,'stopping in File: ', file
        print *,'Line number: ', Nline
        print *,msg
        if (present(val))   print *,' = ',val
        print *,'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        call EXIT(-1)
    end subroutine stopp

end module util

#define BENCH_INCREMENTAL 0
#define BENCH_STEP 1

!! How to add a fortran binding to Open MPI
!! 1. create file in ompi/ompi/mpi/fortran/mpif-h/
!! 2. add file to ompi/ompi/fortran/mpif-h/Makefile.am
!! 3. create symlink in ompi/ompi/mpi/fortran/mpif-h/profile/ with "p" at start of filename ../../../../../ompi/mpi/fortran/mpif-h/${name} p${name}
!! 4. add this symlink in ompi/ompi/mpi/fortran/mpif-h/profile/Makefile.am
!! 5. add interface to ompi/ompi/fortran/use-mpi-ignore-tkr/mpi-ignore-tkr-interfaces.h.in
!! 6. add interface to ompi/ompi/fortran/use-mpi-tkr/mpi-f90-interfaces.h
!! 7. add line to ompi/ompi/fortran/mpif-h/prototypes_mpi.h
!! 8. add line to ompi/ompi/fortran/use-mpi-ignore-tkr/pmpi-ignore-tkr-interfaces.h

program main
    use mpi
    use util
    implicit none

    type :: state_t
        integer :: PROBLEM_SIZE

        integer :: ITER_MAX
        integer :: proc_limit
        integer :: proc_limit_up
        integer :: proc_limit_down
        integer :: num_delta
        integer :: rc_frequency
        integer :: mode_type
        integer :: mode_num
        !integer :: blocking

        integer :: cur_type
        integer :: cur_num_delta
        logical :: check_rc;

        logical :: debug;
    end type state_t

    integer :: num_procs, rank, op
    logical :: contains_key
    integer :: cur_iter = 0, iters_since_last_change = 0
    integer :: start_index
    integer :: end_index
    logical :: dynamic_proc = .false., primary_proc = .false., terminate = .false.

    integer                                  :: rc_type, noutput
    character(len=MPI_MAX_INFO_KEY)          :: keys(1)
    character(len=20)                        :: boolean_string
    character(len=20)                        :: str
    character(len=MPI_MAX_PSET_NAME_LEN)     :: mpi_world_pset = "mpi://WORLD"

    character(len=MPI_MAX_PSET_NAME_LEN)     :: main_pset
    character(len=MPI_MAX_PSET_NAME_LEN)     :: old_main_pset
    character(len=MPI_MAX_PSET_NAME_LEN)     :: delta_pset
    character(len=MPI_MAX_PSET_NAME_LEN)     :: input_psets(10)
    character(len=MPI_MAX_PSET_NAME_LEN)     :: output_psets(10)

    character(len=MPI_MAX_PSET_NAME_LEN)     :: psetop_name

    integer :: comm = MPI_COMM_NULL
    integer :: session_handle = MPI_SESSION_NULL
    integer :: info = MPI_INFO_NULL

    integer :: ierr, dummy
    type(state_t) :: s

    call init_state(s)

    cur_iter = 0

    ! Parse the command line arguments
    call parse_arguments(s)

    ! mpi://WORLD is the initial main PSet
    main_pset = "mpi://WORLD"
    delta_pset = ""

    ! Initialize the session
    call MPI_Session_init(MPI_INFO_NULL, MPI_ERRORS_ARE_FATAL, session_handle, ierr)

    ! Get the info from our mpi://WORLD pset
    call MPI_Session_get_pset_info(session_handle, main_pset, info, ierr)

    ! get value for the 'mpi_dyn' key -> if true, this process was added dynamically
    call MPI_Info_get(info, "mpi_dyn", 20, boolean_string, contains_key, ierr)

    ! if mpi://WORLD is a dynamic PSet retrieve the name of the main PSet stored on mpi://WORLD
    if (contains_key .and. trim(boolean_string) == "True") then
       if (s%debug) print *, "Dynamic start!"
       call MPI_Info_free(info, ierr)

       dynamic_proc = .true.

       keys(1) = "next_main_pset"
       call MPI_Session_get_pset_data(session_handle, mpi_world_pset, mpi_world_pset, keys, 1, 1, info, ierr)
       if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi get pset data fail, error=',ierr)

       call MPI_Info_get(info, "next_main_pset", MPI_MAX_PSET_NAME_LEN, main_pset, contains_key, ierr)

       if (.not. contains_key) then
           if(s%debug) print *, "No 'next_main_pset' was provided for dynamic process. Terminate."
           call MPI_Session_finalize(session_handle, ierr)
           if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi finalize psetop fail, error=',ierr)
           stop
       end if
       call MPI_Info_free(info, ierr)

       if(s%debug) print *,"dynamic, next_main_pset: ", main_pset

       ! Get PSet data stored on main PSet
       call MPI_Session_get_pset_info(session_handle, main_pset, info, ierr)
    end if

    ! Check if this proc is the 'primary process' of the main PSet
    call MPI_Info_get(info, "mpi_primary", 6, boolean_string, contains_key, ierr)

    if (contains_key .and. trim(boolean_string) == "True") then
        primary_proc = .true.
    end if

    call MPI_Info_free(info, ierr)

    ! create a communicator from the main PSet
    call comm_create_from_pset(session_handle, main_pset, comm, num_procs, rank, ierr)

    ! If this is a dynamic process, it needs to recv the current application data
    if (dynamic_proc) then
        call recv_application_data(s, comm, cur_iter, ierr)

        ! If this process is the primary process it needs to finalize the resource change
        if (primary_proc) then
            call MPI_Session_dyn_finalize_psetop(session_handle, main_pset, ierr)
            if(s%debug) print *,"finalize_psetop called"
        end if
    end if

    if (s%debug .AND. primary_proc .AND. .NOT. dynamic_proc) then
       call print_opts(s)
    end if


    do while (cur_iter < s%ITER_MAX)
        cur_iter = cur_iter + 1

        if (rank == 0 .OR. s%debug) then
            write(*,*) 'Start of iteration ', cur_iter, ' with ', num_procs, ' processes in PSet ', main_pset
        endif

        ! App-specific data redistribution
        call rebalance_step(num_procs, rank, start_index, end_index)

        ! App-specific work step
        call work_step(start_index, end_index, dummy)

        call MPI_Barrier(comm, ierr)

        ! Resource Change Step
        iters_since_last_change = iters_since_last_change + 1
        if (s%check_rc .and. iters_since_last_change >= s%rc_frequency) then
           if(s%debug) print *, "Resource change check"

           ! Check for a resource change related to the main PSet (e.g. from RM)
           ! set noutput to size of output_psets
           noutput = 10
           call MPI_Session_dyn_v2a_query_psetop(session_handle, main_pset, main_pset, rc_type, output_psets, noutput, ierr)
           if (ierr /=0) call stopp(__FILE__,__LINE__,'1st mpi session dyn v2a query psetop fail, error=',ierr)
           call psetop2str(rc_type, psetop_name)
           if(s%debug) print *, "Called query psetop returned with ", noutput, " psets and ", psetop_name

           if (MPI_PSETOP_NULL == rc_type) then
               ! In this example the 'primary process' requests set operations
               if (primary_proc) then
                   call MPI_Info_create(info, ierr)

                   write (str, "(I4)") s%cur_num_delta
                   if (s%cur_type == MPI_PSETOP_ADD) then
                      call MPI_Info_set(info, "mpi_num_procs_add", str, ierr)
                   else
                      call MPI_Info_set(info, "mpi_num_procs_sub", str, ierr)
                   endif

                   noutput = 0
                   input_psets(1) = main_pset
                   ! Request ADD / SUB operation
                   if (s%cur_type == MPI_PSETOP_ADD) then
                       op = MPI_PSETOP_GROW
                   else
                       op = MPI_PSETOP_SHRINK
                   end if

                   ! set noutput to size of output_psets
                   noutput = 10
                   call MPI_Session_dyn_v2a_psetop(session_handle, op, input_psets, 1, output_psets, noutput, info, ierr)
                   if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi session dyn v2a psetop fail, error=',ierr)
                   call psetop2str(op, psetop_name)
                   if(s%debug) print *, "Called psetop returned with ", noutput, " psets and ", psetop_name

                   call MPI_Info_free(info, ierr)

                   if (MPI_PSETOP_NULL /= op) then
                       ! Publish the name of the new main PSet on the delta Pset
                       call MPI_Info_create(info, ierr)
                       call MPI_Info_set(info, "next_main_pset", output_psets(2), ierr)
                       call MPI_Session_set_pset_data(session_handle, output_psets(1), info, ierr)
                       if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi set pset data fail, error=',ierr)
                       if(s%debug) print *, "set_pset_data called"
                       call MPI_Info_free(info, ierr)
                   endif
               endif



               call MPI_Barrier(comm, ierr)
               ! Now again query for the Set operation info
               ! set noutput to size of output_psets
               noutput = 10
               call MPI_Session_dyn_v2a_query_psetop(session_handle, main_pset, main_pset, rc_type, output_psets, noutput, ierr)
               if (ierr /=0) call stopp(__FILE__,__LINE__,'2nd mpi session dyn v2a query psetop fail, error=',ierr)
               call psetop2str(rc_type, psetop_name)
               if(s%debug) print *, "2nd time: Called query psetop returned with ", noutput, " psets and ", psetop_name
           endif

           ! Continue work if no new PSETOP was found
           if (MPI_PSETOP_NULL == rc_type .or. delta_pset == output_psets(1)) then
              if(s%debug) print *, "Continue since no new psetop"
              iters_since_last_change = 0
              cycle
           endif

           if(s%debug) print *, "new psetop!"
           delta_pset = output_psets(1)

           ! Is proc included in the delta PSet? If yes, need to terminate
           call MPI_Session_get_pset_info(session_handle, output_psets(1), info, ierr)
           call MPI_Info_get(info, "mpi_included", 6, boolean_string, contains_key, ierr)
           if (contains_key .and. boolean_string == "True") then
              if(s%debug) print *, "delta pset contains me, need to stop"
              terminate = .true.
           endif
           call MPI_Info_free(info, ierr)

           ! Get the name of the new main PSet stored on the delta PSet
           old_main_pset = main_pset
           keys(1) = "next_main_pset"
           call MPI_Session_get_pset_data(session_handle, main_pset, output_psets(1), keys, 1, 1, info, ierr)
           if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi get pset data fail, error=',ierr)
           if(s%debug) print *, "get pset data called"

           call MPI_Info_get(info, "next_main_pset", MPI_MAX_PSET_NAME_LEN, main_pset, contains_key, ierr)
           if(s%debug) print *, "new main pset:", main_pset

           if (.not. contains_key) then
               write(*,*) 'could not find next_main_pset on PSet ', main_pset, '. This should never happen! Terminate.'
               stop 1
           endif

           call MPI_Info_free(info, ierr)

           ! Is this proc the primary process of the new main PSet? Primary proc could have changed!
           call MPI_Session_get_pset_info(session_handle, main_pset, info, ierr)
           call MPI_Info_get(info, "mpi_primary", 6, boolean_string, contains_key, ierr)
           primary_proc = (boolean_string == "True")
           call MPI_Info_free(info, ierr)

           ! This process is included in the delta PSet => it need to terminate
           if (terminate) then
               exit
           endif
           ! Disconnect from the old communicator
           call MPI_Comm_disconnect(comm, ierr)

           ! create a new communicator from the new main PSet
           call comm_create_from_pset(session_handle, main_pset, comm, num_procs, rank, ierr)

           ! Update and send the application parameters
           call eval_parameters(s, num_procs)
           call send_application_data(s, comm, rank, cur_iter, ierr)

           if (primary_proc) then
               call MPI_Session_dyn_finalize_psetop(session_handle, old_main_pset, ierr)
               if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi finalize psetop fail, error=',ierr)
               if(s%debug) print *,"finalize_psetop called"
           endif

           ! Reset the counter since last resource change
           iters_since_last_change = 0
        endif

    end do ! END OF MAIN APPLICATION LOOP


    ! Disconnect from the communicator
    call MPI_Comm_disconnect(comm, ierr)

    ! Finalize the session
    call MPI_Session_finalize(session_handle, ierr)
    if (rank == 0) then
        write(*,*) 'finished benchmark successfully!'
    endif

contains
    subroutine init_state(s)
      implicit none
      type(state_t), intent(inout) :: s
      s%PROBLEM_SIZE = 100000000

      s%ITER_MAX = 200
      s%proc_limit = 64
      s%proc_limit_up = 64
      s%proc_limit_down = 1
      s%num_delta = 8
      s%rc_frequency = 10
      s%mode_type = MPI_PSETOP_ADD
      s%mode_num = BENCH_INCREMENTAL
      !s%blocking = 0
      s%debug = .FALSE.
    end subroutine init_state

    subroutine set_mode(s, c_mode, rc)
        type(state_t), intent(inout) :: s
        character(len=*), intent(in) :: c_mode
        integer, intent(out) :: rc

        if (len_trim(c_mode) /= 2) then
            rc = -1
            return
        endif

        if (c_mode(1:1) == 'i') then
            s%mode_num = BENCH_INCREMENTAL
        else if (c_mode(1:1) == 's') then
            s%mode_num = BENCH_STEP
        else
            rc = -1
            return
        endif

        s%cur_num_delta = s%num_delta

        if (c_mode(2:2) == '+') then
            s%mode_type = MPI_PSETOP_ADD
            s%proc_limit_up = s%proc_limit
        else if (c_mode(2:2) == '_') then
            s%mode_type = MPI_PSETOP_SUB
            s%proc_limit_down = s%proc_limit
        else
            rc = -1
            return
        endif

        s%cur_type = s%mode_type

        s%check_rc = .TRUE.

        rc = 0
    end subroutine set_mode

    subroutine eval_parameters(s, current_size)
        type(state_t), intent(inout) :: s
        integer, intent(in) :: current_size

        select case(s%mode_num)
            case(BENCH_STEP)
                if (s%cur_type == MPI_PSETOP_ADD) then
                  s%cur_type = MPI_PSETOP_SUB
                else
                  s%cur_type = MPI_PSETOP_ADD
                end if

                if (s%cur_type == s%mode_type) then
                   s%cur_num_delta = s%cur_num_delta + s%num_delta
                end if

                if (s%cur_num_delta <= 0) then
                    s%check_rc = .false.
                else
                    if (s%cur_type == MPI_PSETOP_ADD) then
                        s%check_rc = ((current_size + s%cur_num_delta) <= s%proc_limit_up)
                    else
                        s%check_rc = ((current_size - s%cur_num_delta) >= s%proc_limit_down)
                    end if
                end if

            case(BENCH_INCREMENTAL)
                if (s%mode_type == MPI_PSETOP_ADD) then
                    s%check_rc = (current_size + s%cur_num_delta <= s%proc_limit_up)
                else
                    s%check_rc =  (current_size - s%cur_num_delta >= s%proc_limit_down)
                end if

            case default
                ! Do nothing
        end select
    end subroutine

    subroutine send_application_data(s, comm, rank, cur_iter, rc)
        type(state_t), intent(inout) :: s
        integer, intent(in) :: comm
        integer, intent(in) :: rank
        integer, intent(in) :: cur_iter
        integer, intent(out) :: rc
        integer, dimension(4) :: buf

        if (rank == 0) then
            buf(1) = cur_iter
            buf(2) = s%cur_type
            buf(3) = s%cur_num_delta
            if (s%check_rc) then
                buf(4) = 1
            else
                buf(4) = 0
            end if
        end if

        call mpi_bcast(buf, 4, MPI_INT, 0, comm, rc)
    end subroutine

    subroutine recv_application_data(s, comm, cur_iter, rc)
        type(state_t), intent(inout) :: s
        integer, intent(in) :: comm
        integer, intent(out) :: cur_iter
        integer, intent(out) :: rc
        integer, dimension(4) :: buf

        call mpi_bcast(buf, 4, MPI_INT, 0, comm, rc)

        if (rc == MPI_SUCCESS) then
            cur_iter = buf(1)
            s%cur_type = buf(2)
            s%cur_num_delta = buf(3)
            if (buf(4) /= 0) then
               s%check_rc = .TRUE.
            else
               s%check_rc = .FALSE.
            end if
        end if
    end subroutine

    subroutine parse_arguments(s)
        implicit none
        type(state_t), intent(inout) :: s

        character(len=30) :: mode_s
        integer :: i, n, rc
        character(len=30) :: arg
        character(len=30) :: prog_name
        mode_s = ""

        call get_command_argument(0, prog_name) ! get the ith command line argument

        n = command_argument_count() ! get number of command line arguments

        i = 1
        do while (i <= n)
            call get_command_argument(i, arg) ! get the ith command line argument

            !if (arg == "-b") then
            !    i = i + 1
            !    call get_command_argument(i, arg)
            !    s%blocking = atoi(arg)
            if (arg == "-h") then
                print *, "Usage: ", prog_name, &
                     " [-c <ITER_MAX>] [-l <proc_limit>] [-n <num_delta>] " // &
                     "[-f <rc_frequency>] [-m <mode_string>]"
                print *, ""
                print *, "Options:"
                print *, "  -c <ITER_MAX>       Maximum number of iterations (default: 200)"
                print *, "  -l <proc_limit>     Maximum number of processors (default: 64)"
                print *, "  -n <num_delta>      Number of delta values (default: 8)"
                print *, "  -f <rc_frequency>   Frequency of resource change steps (default: 10)"
                print *, "  -m <mode_string>    Mode (default: i+)"
                stop
            elseif (arg == "-c") then
                i = i + 1
                call get_command_argument(i, arg)
                s%ITER_MAX = atoi(arg)
            elseif (arg == "-l") then
                i = i + 1
                call get_command_argument(i, arg)
                s%proc_limit = atoi(arg)
            elseif (arg == "-n") then
                i = i + 1
                call get_command_argument(i, arg)
                s%num_delta = atoi(arg)
            elseif (arg == "-f") then
                i = i + 1
                call get_command_argument(i, arg)
                s%rc_frequency = atoi(arg)
            elseif (arg == "-d") then
                s%debug = .TRUE.
            elseif (arg == "-m") then
                i = i + 1
                call get_command_argument(i, arg)
                mode_s = arg
            else
                print *, "Unknown option: ", arg
            end if

            i = i+1
        end do

        ! Set the mode if provided
        if (len_trim(mode_s) > 0) then
            call set_mode(s, mode_s, rc)
        endif

    end subroutine parse_arguments

    subroutine print_opts(s)
        implicit none
        type(state_t), intent(inout) :: s

        print *, "------------ OPTIONS ------------"
        !print *, "blocking:      ", s%blocking
        print *, "iter_max:      ", s%ITER_MAX
        print *, "proc_limit:    ", s%proc_limit
        print *, "num_delta:     ", s%num_delta
        print *, "rc_frequency:  ", s%rc_frequency
        print *, "mode_type:     ", s%mode_type ! TODO
        print *, "mode_num:      ", s%mode_num  ! TODO
        print *, "---------------------------------"
    end subroutine print_opts

    subroutine comm_create_from_pset(session, pset_name, comm, num_procs, rank, rc)
        integer, intent(in) :: session
        character(len=*), intent(in)  :: pset_name
        integer, intent(out) :: comm
        integer, intent(out) :: num_procs
        integer, intent(out) :: rank
        integer, intent(out) :: rc

        integer :: ierr
        integer :: wgroup

        call mpi_group_from_session_pset(session, pset_name, wgroup, ierr)
        if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi group from pset fail, error=',ierr)

        call mpi_comm_create_from_group(wgroup, "mpi.forum.example", MPI_INFO_NULL, MPI_ERRORS_RETURN, comm, ierr)
        if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi comm create from group fail, error=',ierr)

        call mpi_group_free(wgroup, ierr)
        if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi group free fail, error=',ierr)

        call mpi_comm_size(comm, num_procs, ierr)
        if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi comm size fail, error=',ierr)

        call mpi_comm_rank(comm, rank, ierr)
        if (ierr /=0) call stopp(__FILE__,__LINE__,'mpi comm rank fail, error=',ierr)

        rc = MPI_SUCCESS
    end subroutine comm_create_from_pset

    subroutine work_step(start_index, end_index, i)
        integer, intent(in) :: start_index
        integer, intent(in) :: end_index
        integer, intent(out) :: i

        integer :: n;
        real :: a,b,c,d,e,f,t,x,y
        a = 1.57
        b = 20.1113
        c = 5.4200102
        d = 5.223
        e = 1.89
        f = 22.223

        i = 0

        do n = start_index, end_index
            t = n
            x = a*t*t + b*t + c
            y = d*t*t + e*t + f

            if (x > 42.42 .AND. x < 42.4242 .AND. y > 42.42 .AND. y < 42.4242) then
                i = i + 1
            end if
        end do
    end subroutine work_step

    subroutine rebalance_step(num_procs, my_work_rank, start_index, end_index)
        integer, intent(in) :: num_procs
        integer, intent(in) :: my_work_rank
        integer, intent(out) :: start_index
        integer, intent(out) :: end_index
        integer :: chunk_size

        chunk_size = s%PROBLEM_SIZE / num_procs
        start_index = chunk_size * my_work_rank
        if (my_work_rank == num_procs - 1) then
            end_index = s%PROBLEM_SIZE
        else
            end_index = (my_work_rank + 1) * chunk_size
        end if
    end subroutine

    subroutine psetop2str(psetop, str)
      integer, intent(in) :: psetop
      character(len=*), intent(out) :: str

      character(len=23) :: myStrings(11) = [ &
        "MPI_PSETOP_NULL        ", &
        "MPI_PSETOP_ADD         ", &
        "MPI_PSETOP_SUB         ", &
        "MPI_PSETOP_REPLACE     ", &
        "MPI_PSETOP_MALLEABLE   ", &
        "MPI_PSETOP_GROW        ", &
        "MPI_PSETOP_SHRINK      ", &
        "MPI_PSETOP_UNION       ", &
        "MPI_PSETOP_DIFFERENCE  ", &
        "MPI_PSETOP_INTERSECTION", &
        "MPI_PSETOP_MULTI       " &
      ]

      str = mystrings(psetop+1)
    end subroutine psetop2str

end program main

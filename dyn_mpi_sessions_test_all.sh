#! /bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

SUFFIX=${1:-"release"}

echo "RUNNING TEST='VERSION 2A', MODE='i+', start_procs=8, end_procs=32 procs, delta_procs=8"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map  --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_$SUFFIX -c 120 -l 32 -m i+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A', MODE='i-', start_procs=32, end_procs=8 procs, delta_procs=8"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_$SUFFIX -c 120 -l 1 -m i_ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A', MODE='s+', start_procs=8, end_procs=32 procs, delta_procs=8,16,24"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_$SUFFIX -c 200 -l 32 -m s+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A', MODE='s_', start_procs=32, end_procs=8 procs, delta_procs=8,16,24"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_$SUFFIX -c 250 -l 1 -m s_ -n 8 -f 10 -b 0



echo "RUNNING TEST='VERSION 2A_NB', MODE='i+', start_procs=8, end_procs=32 procs, delta_procs=8"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map  --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_nb_$SUFFIX -c 120 -l 32 -m i+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A_NB', MODE='i-', start_procs=32, end_procs=8 procs, delta_procs=8"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_nb_$SUFFIX -c 120 -l 1 -m i_ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A_NB', MODE='s+', start_procs=8, end_procs=32 procs, delta_procs=8,16,24"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_nb_$SUFFIX -c 200 -l 32 -m s+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A_NB', MODE='s_', start_procs=32, end_procs=8 procs, delta_procs=8,16,24"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_nb_$SUFFIX -c 250 -l 1 -m s_ -n 8 -f 10 -b 0



echo "RUNNING TEST='VERSION 2B', MODE='i+', start_procs=8, end_procs=32 procs, delta_procs=8"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map  --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2b_$SUFFIX -c 120 -l 32 -m i+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2B', MODE='i-', start_procs=32, end_procs=8 procs, delta_procs=8"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2b_$SUFFIX -c 120 -l 1 -m i_ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2B', MODE='s+', start_procs=8, end_procs=32 procs, delta_procs=8,16,24"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2b_$SUFFIX -c 200 -l 32 -m s+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2B', MODE='s_', start_procs=32, end_procs=8 procs, delta_procs=8,16,24"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2b_$SUFFIX -c 250 -l 1 -m s_ -n 8 -f 10 -b 0



echo "RUNNING TEST='VERSION 2B_NB', MODE='i+', start_procs=8, end_procs=32 procs, delta_procs=8"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map  --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2b_nb_$SUFFIX -c 120 -l 32 -m i+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2B_NB', MODE='i-', start_procs=32, end_procs=8 procs, delta_procs=8"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2b_nb_$SUFFIX -c 120 -l 1 -m i_ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2B_NB', MODE='s+', start_procs=8, end_procs=32 procs, delta_procs=8,16,24"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2b_nb_$SUFFIX -c 200 -l 32 -m s+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2B_NB', MODE='s_', start_procs=32, end_procs=8 procs, delta_procs=8,16,24"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2b_nb_$SUFFIX -c 250 -l 1 -m s_ -n 8 -f 10 -b 0



echo "RUNNING TEST='VERSION 2A FORTRAN', MODE='i+', start_procs=8, end_procs=32 procs, delta_procs=8"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map  --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_fortran_$SUFFIX -c 120 -l 32 -m i+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A FORTRAN', MODE='i-', start_procs=32, end_procs=8 procs, delta_procs=8"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_fortran_$SUFFIX -c 120 -l 1 -m i_ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A FORTRAN', MODE='s+', start_procs=8, end_procs=32 procs, delta_procs=8,16,24"

mpirun -np 8 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_fortran_$SUFFIX -c 200 -l 32 -m s+ -n 8 -f 10 -b 0

echo "RUNNING TEST='VERSION 2A FORTRAN', MODE='s_', start_procs=32, end_procs=8 procs, delta_procs=8,16,24"

mpirun -np 32 --host n1:8,n2:8,n3:8,n4:8 --mca gds hash --map-by slot --bind-to none --display map --mca btl_tcp_if_include eth0  -x LD_LIBRARY_PATH -x DYNMPI_BASE $SCRIPT_DIR/build/DynMPISessions_v2a_fortran_$SUFFIX -c 250 -l 1 -m s_ -n 8 -f 10 -b 0

# Time-X EuroHPC project: Test applications

This repository contains some simple, synthetic test applications for the [TIME-X Dynamic MPI](https://gitlab.inria.fr/dynres/dyn-procs/ompi)

## Run the examples

1. Use the SConstruct script to build the test applications:

```scons example=DynMPISessions_v[2a/2a_nb/2b/2b_nb] compileMode=[release/debug]``` 

2. run the batch scripts or use the prterun/mpirun commands from the batch script to run the application
